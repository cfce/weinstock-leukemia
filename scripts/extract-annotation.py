#!/usr/bin/env python3

import argparse
import re
import sys

import yaml


def main(regex="(?P<type>[A-Za-z]+)[0-9]+[_-](?P<batch>[^_-]+)"):
    p = argparse.ArgumentParser(description="Extract annotation from sample names.")
    p.add_argument("config", help="Workflow config file in YAML format.")
    p.add_argument("--regex", default=regex, help="Regular expression with two groups: type and batch. Default: '{}'".format(regex))

    args = p.parse_args()

    regex = re.compile(regex)

    with open(args.config) as f:
        config = yaml.load(f)
        print("sample", "type", "batch", sep="\t")
        for sample in config["samples"]:
            match = regex.search(sample)
            if match is None:
                print("Could not match sample", sample, file=sys.stderr)
            else:
                print(sample, match.group("type"), match.group("batch"), sep="\t")


if __name__ == "__main__":
    main()

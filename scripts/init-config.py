#!/usr/bin/env python3

import sys
import os
import re
import glob
import argparse
from itertools import groupby
from functools import partial
from collections import defaultdict
import yaml


def get_sample(bam_path, regex=None, fix=False):
    match = regex.search(os.path.basename(bam_path))
    if match is not None:
        return match.group("sample").replace("_", "-")
    print("No sample name found in", bam_path, file=sys.stderr)


def main():
    p = argparse.ArgumentParser(description="Create config file from template and bam files.")
    p.add_argument("transcripts", help="Gene and transcript annotation file in gtf format.")
    p.add_argument("reference", help="Path to reference genome sequence.")
    p.add_argument("bamfiles", nargs="+", help="Path to bam files.")
    p.add_argument("--sample-regex", default="^(?P<sample>[a-zA-Z0-9]+[_-][0-9]+).*\.bam$", help="Regular expression to extract sample name from given bam filenames.")
    p.add_argument("--fix-names", action="store_true", help="Replace all underscores by hyphens.")

    args = p.parse_args()

    _get_sample = partial(get_sample, regex=re.compile(args.sample_regex), fix=args.fix_names)

    config = {"reference": args.reference, "transcripts": args.transcripts, "samples": defaultdict(list), "replicates": {}}

    for sample, bams in groupby(sorted(sorted(args.bamfiles, key=os.path.getmtime), key=_get_sample),
                                key=_get_sample):
        for i, bam in enumerate(bams):
            replicate = "{}_{}".format(sample, i)
            config["replicates"][replicate] = bam
            config["samples"][sample].append(replicate)
    config["samples"] = dict(config["samples"])

    print(yaml.dump(config, default_flow_style=False))


if __name__ == "__main__":
    main()

import pandas as pd
import numpy as np
import matplotlib as mpl
mpl.use("agg")
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib_venn import venn2, venn3


def drop_duplicates(expr):
    return expr[np.logical_not(expr.index.duplicated())]


def merge_gene_expressions(expr_path, rpkm_paths, rma_paths,
                           annotation_path,
                           labels, output_path, overlap_path,
                           sample_sources_path):
    annotation = pd.read_table(annotation_path, index_col=0)
    whitelist = annotation[annotation["Include_in_Analysis_and_Figures"] == 1].index

    # we log2-scale all RPKM values to be comparable with RMA intensities
    expr = np.log2(pd.read_table(expr_path, index_col=0) + 1)
    ext_rpkms = [np.log2(drop_duplicates(pd.read_table(f,
                                                       index_col=0)) + 1)
                 for f in rpkm_paths]
    # RMA is already in log2-scale
    ext_rma = [drop_duplicates(pd.read_table(f,
                                             index_col=0)) for f in rma_paths]

    tocombine = [expr] + ext_rpkms + ext_rma
    innerjoin = pd.concat(tocombine, axis=1, join="inner")
    innerjoin.index.name = "gene"

    innerjoin = innerjoin.loc[:, whitelist].dropna(axis="columns")

    innerjoin.to_csv(output_path, sep="\t")

    ## plot overlap
    genes = lambda: [set(d.index) for d in tocombine]
    plt.title("Common and exclusive genes.")
    if len(tocombine) == 2:
        venn2(genes(), set_labels=labels)
    elif len(tocombine) == 3:
        venn3(genes(), set_labels=labels)
    else:
        genes = genes()
        common = genes[0]
        for g in genes[1:]:
            common &= g
        exclusive = [g - common for g in genes]
        common = len(common)
        exclusive = np.fromiter(map(len, exclusive), dtype=np.int32)
        x = np.arange(len(exclusive))
        sns.set_style("ticks")
        plt.bar(x, exclusive + common, 0.5)
        plt.plot([0, x[-1] + 0.5], [common] * 2, "--k")
        plt.text(x[-1] + 0.6, common, "common", verticalalignment="center")
        plt.xlim([0, x[-1] + 2])
        plt.xticks(x + 0.5 / 2, labels)
        plt.ylabel("genes")
        sns.despine()
    plt.savefig(overlap_path, bbox_inches="tight")

    ## output sources (TODO get rid of this)
    samples = []
    _labels = []
    for d, label in zip(tocombine, labels):
        samples.extend(d.columns)
        _labels.extend([label] * len(d.columns))
    pd.Series(_labels, index=samples).to_csv(sample_sources_path, sep="\t")


merge_gene_expressions(
    snakemake.input.expr, snakemake.input.rpkm, snakemake.input.rma,
    snakemake.input.annotation,
    snakemake.params.labels, snakemake.output.txt,
    snakemake.output.overlap, snakemake.output.sample_sources)

import pandas as pd
import matplotlib as mpl
mpl.use("agg")
import matplotlib.pyplot as plt
import seaborn as sns

def plot_expression_dist(expr_path, annotation_path, output_path):
    expr = pd.read_table(expr_path, index_col=0)
    annotation = pd.read_table(annotation_path, index_col=0)
    #sources = pd.Series.from_csv(sources_path, sep="\t")
    cohorts = annotation["cohort"].unique()
    colors = {cohort: c for cohort, c in zip(cohorts, sns.color_palette("muted", n_colors=cohorts.shape[0]))}

    sns.set(style="ticks", palette="colorblind")
    visited = set()
    for sample in expr.columns:
        cohort = annotation.loc[sample, "cohort"]
        sns.distplot(expr[sample], hist=False, label=cohort if cohort not in visited else None, color=colors[cohort], kde_kws={"alpha": 0.5, "lw": 1})
        visited.add(cohort)
    plt.xlim([0, plt.xlim()[1]])
    plt.xlabel("log2 expression")
    plt.ylabel("density")
    sns.despine()
    plt.savefig(output_path, bbox_inches="tight")


plot_expression_dist(snakemake.input.expr, snakemake.input.annotation, snakemake.output[0])

import pandas as pd
import numpy as np



def get_matrix(expr_path, annotation_path, out, cohort):
    log2expr = pd.read_table(expr_path, index_col=0)
    annotation = pd.read_table(annotation_path, index_col=0)

    samples = annotation[annotation["cohort"] == cohort].index

    log2expr = log2expr.loc[:, samples].dropna(axis="columns")
    rpkm = np.exp2(log2expr)

    rpkm.to_csv(out, sep="\t")


get_matrix(snakemake.input.expr, snakemake.input.annotation,
           snakemake.output[0], snakemake.wildcards.cohort)

#!/usr/bin/env python3

import re
import argparse
import sys
from itertools import combinations

import numpy as np
import matplotlib as mpl
mpl.use("agg")
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib import gridspec
import seaborn as sns
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale


def plot_pca(expressions, out, annotation=None, annotation_column=None):
    expressions = pd.read_table(expressions, index_col=0)

    annotation = pd.read_table(annotation,
                               index_col=0,
                               dtype=object,
                               na_values=[""])
    annotation = annotation.loc[expressions.columns]

    pca = PCA(n_components=3)
    X = expressions
    X = X.transpose()
    X = scale(X)
    scores = pd.DataFrame(pca.fit_transform(X))
    scores.columns = [
        "PC{} ({:.2%})".format(i + 1, expl_var)
        for i, expl_var in enumerate(pca.explained_variance_ratio_)
    ]
    scores.index = expressions.columns
    pcs = list(combinations(scores.columns, 2))

    sns.set(style="ticks", palette="colorblind")
    fig = plt.figure(figsize=(9, 3))
    gs = gridspec.GridSpec(1, 3)

    for i, (a, b) in enumerate(pcs):
        ax = plt.subplot(gs[0, i])
        for group, df in annotation.groupby(annotation_column):
            ax.plot(scores.loc[df.index.values, a],
                    scores.loc[df.index.values, b], ".",
                    label=group)
        if i == 2:
            ax.legend(bbox_to_anchor=(1.6, 1))
        plt.xlabel(a)
        plt.ylabel(b)

    # save the figure
    sns.despine()
    plt.tight_layout()
    fig.savefig(out, bbox_inches="tight")


plot_pca(snakemake.input.expr, snakemake.output[0],
     annotation=snakemake.input.annotation,
     annotation_column=snakemake.params.annotation_column)

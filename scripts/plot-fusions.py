from itertools import product

import numpy as np
import pandas as pd
import matplotlib as mpl
mpl.use("agg")
import matplotlib.pyplot as plt
import seaborn as sns


def plot_fusions(fusions_path, annotation_path, whitelist_path, txt_path, pdf_path, covariate):
    fusions = pd.read_table(fusions_path, index_col=1)
    fusions = fusions[np.logical_and(fusions["sum_spanning"] >= 1,
                                     fusions["sum_junction"] >= 1)]
    annotation = pd.read_table(annotation_path, index_col=0)

    whitelist = list(pd.read_table(whitelist_path, squeeze=True).unique())

    n = fusions["fusion"].unique().shape[0]
    fusions.reset_index(inplace=True)

    def select_candidate(fusions):
        # for the same fusion, select the candidate with most support
        fusions = fusions.sort("spanning-fragments", ascending=False)
        fusions = fusions.loc[fusions["junction-reads"] ==
                              fusions["junction-reads"].max(),
                              ["junction-reads", "spanning-fragments"]]
        if fusions.shape[0] > 1:
            fusions = fusions.loc[:1]
        return fusions

    fusions = fusions.groupby(["fusion", "sample"],
                              sort=False).apply(select_candidate)

    sns.set_style("ticks")
    fusions.index = fusions.index.droplevel(2)
    fusions = fusions["junction-reads"] + fusions["spanning-fragments"]
    fusions = fusions.reindex(list(product(*fusions.index.levels))).unstack()
    idx = fusions.isnull().sum(axis="columns").order().index
    fusions = fusions.loc[idx]
    fusions = fusions[fusions.sum(axis="columns") >= 10]
    fusions = fusions[~fusions.index.str.contains("---") & ~fusions.index.str.contains("LOC")]
    
    unknown = set(whitelist) - set(fusions.index)
    if unknown:
        print("Warning: unknown fusions in whitelist:", *unknown, sep="\n")

    # only keep samples where type equals covariate
    fusions = fusions.reindex(columns=annotation[(annotation["group"] == covariate) & (annotation["Include_in_Analysis_and_Figures"] == 1)].index).dropna(axis="columns", how="all")
    present = fusions.sum(axis="columns") > 0
    fusions = fusions[present]

    fusions.columns = annotation.loc[fusions.columns, "label"]
    fusions = fusions.sort_index(axis="columns")
    fusions["total"] = fusions.sum(axis="columns")
    
    fusions = fusions.loc[fusions.index.intersection(whitelist)]
    prevalence_order = (fusions > 0).sum(axis="columns").sort_values(inplace=False, ascending=False).index
    fusions = fusions.loc[prevalence_order]


    # Move reciprocal fusions below corresponding with higher prevalence
    fusion_order = list(fusions.index)
    visited = set()
    for fusion in list(fusion_order):
        reciprocal = "--".join(fusion.split("--")[::-1])
        if reciprocal in visited:
            continue
        try:
            i = fusion_order.index(reciprocal)
        except ValueError:
            continue
        j = fusion_order.index(fusion) + 1
        fusion_order.pop(i)
        fusion_order.insert(j, reciprocal)
        visited.add(fusion)
        print(fusion_order)

    fusions = fusions.loc[fusion_order]

    fusions.to_csv(txt_path, sep="\t")

    if not fusions.empty:
        fusions = np.log10(fusions)
        plt.figure(figsize=(fusions.shape[1] / 5 + 4, fusions.shape[0] / 5))
        ax = sns.heatmap(fusions,
                         robust=True,
                         cbar_kws={"label": "log10 evidence (junction reads + spanning fragments)"},
                         linewidths=0.5,
                         linecolor="grey")
        plt.xticks(rotation=45, ha="right")
        plt.yticks(fontstyle='italic')

    plt.savefig(pdf_path, bbox_inches="tight")


plot_fusions(snakemake.input.fusions, snakemake.input.annotation,
             snakemake.input.whitelist,
             snakemake.output.txt, snakemake.output.pdf,
             snakemake.wildcards.covariate)

import pandas as pd
import numpy as np

def calc_gene_expressions(expr_paths, names, output_path):
    data = []
    for i, (name, rep) in enumerate(zip(names, expr_paths)):
        d = pd.read_table(rep, index_col="tracking_id", usecols=["tracking_id", "gene_id", "gene_short_name", "length", "FPKM"], na_values="-")
        if d["gene_short_name"].isnull().all():
            d.drop("gene_short_name", axis="columns", inplace=True)
        else:
            d.drop("gene_id", axis="columns", inplace=True)
        d.columns = ["gene", "length", name]
        # we filter out all transcripts with length smaller than 300 because according to
        # Colin Trapnell (http://seqanswers.com/forums/archive/index.php/t-17404.html)
        # they are not reliably quantifiable in RNA-Seq. This includes Snornas and Mirnas.
        d = d[np.logical_and(d["gene"] != "-", d["length"] >= 300)]
        d = d[["gene", name]]

        # we sum transcript FPKMs to obtain gene level FPKMs (similar to http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3334321)
        d = d.groupby("gene").aggregate(np.sum)
        data.append(d)
    data = pd.concat(data, axis=1, join="inner")
    data.to_csv(output_path, sep="\t")


calc_gene_expressions(snakemake.input, snakemake.params.names, snakemake.output[0])

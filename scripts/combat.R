#!/usr/bin/env Rscript

# reads expressions from stdin
# first arg: batch effect column in annotation file
# second arg: annotation file

library(sva)

remove_batch_effect <- function(exprs, annotation, batch.column, output) {
    annotation <- read.table(annotation, header = TRUE, row.names = 1, sep = "\t", quote = "")

    # read from stdin
    exprs.log2 <- read.table(exprs, header = TRUE, row.names = 1, check.names = FALSE, sep = "\t", quote = "")

    # get types
    annotation <- data.frame(annotation[colnames(exprs.log2), ])

    stopifnot(nrow(annotation) == ncol(exprs.log2))

    # get known batches
    batches <- factor(annotation[, batch.column])

    if(nlevels(batches) == 1) {
        # just copy the given data
        pdf(file = output)
        dev.off()
        sink()
        exprs.log2.nobatch <- data.frame(exprs.log2, check.names = FALSE)
        exprs.log2.nobatch <- cbind(gene = rownames(exprs.log2.nobatch), exprs.log2.nobatch)
        write.table(exprs.log2.nobatch, file = snakemake@output[["txt"]], sep="\t", quote = FALSE, row.names = FALSE)
    } else {
        # create intercept model (needed by combat, here indicating that no other covariates are involved)
        model <- model.matrix(~1, data = annotation)
        #model <- model.matrix(~as.factor(Primary_Biologic_Driver_Alteration), data = annotation)

        # convert to matrix
        exprs.log2 <- as.matrix(exprs.log2)

        # perform batch correction. Small values can become < 0. It is safe to max them
        # away: https://support.bioconductor.org/p/53068/
        pdf(file = snakemake@output[["pdf"]])
        exprs.log2.nobatch <- pmax(ComBat(exprs.log2, batches, mod = model, prior.plots = TRUE), 0.0)
        dev.off()

        # format data.frame
        exprs.log2.nobatch <- data.frame(exprs.log2.nobatch, check.names = FALSE)
        exprs.log2.nobatch <- cbind(gene = rownames(exprs.log2.nobatch), exprs.log2.nobatch)

        sink()
        # write to stdout
        write.table(exprs.log2.nobatch, file = snakemake@output[["txt"]], sep="\t", quote = FALSE, row.names = FALSE)
    }
}

remove_batch_effect(
    snakemake@input[["expr"]],
    snakemake@input[["annotation"]],
    snakemake@params[["batch_column"]],
    snakemake@output[["pdf"]]
)

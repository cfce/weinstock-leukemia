#!/usr/bin/env python
import numpy as np
import pandas as pd
import yaml

with open("config/config.yaml") as f:
    config = yaml.load(f)

gene_annotation = pd.read_excel(config["annotation"], "gene_annotation").dropna(subset=["gene"])
cytogenetics_annotation = pd.read_excel(config["annotation"], "cytogenetics_annotation").dropna(subset=["alteration"])

annotation = pd.read_excel(config["annotation"], "sample_annotation").dropna(subset=["sample"])

for col in annotation.columns:
    if annotation[col].dtype == np.object_:
        print("Normalizing strings.")
        annotation[col] = annotation[col].str.strip()

annotation["AML_Cytogenetic_Risk_Category"] = annotation["AML_Cytogenetic_Risk_Category"].str.replace("^(\[Not Available\]|Not Available)$", "Unable to assess", case=False)
                                             

# subset and order mutated genes
for gene in gene_annotation["gene"]:
    if "{}.type" in annotation.columns:
        # set non-curated mutations (type != 1) to NA
        annotation.loc[annotation["{}.binary".format(gene)] != 1, "{}.type".format(gene)] = None
# extract and order mutations
genes = annotation.loc[:, ["{}.type".format(gene) for gene in gene_annotation["gene"]]]
# remove all .type columns from annotation
annotation = annotation.drop(annotation.columns[annotation.columns.str.endswith(".type")], axis="columns")
assert annotation.shape[0] == genes.shape[0]
# add selected .type columns to annotation
annotation = annotation.join(genes)

# merge and subset cytogenetic alterations
merged = pd.DataFrame()
for label, group in cytogenetics_annotation.groupby("label", sort=False):
    group = group.loc[group["show"] == 1, "alteration"]
    if not group.empty:
        v = annotation.loc[:, group.values].any(axis="columns")
        v[v] = 1
        v[~v] = None
        merged["Cytogenetics_{}".format(label)] = v
annotation = annotation.drop(annotation.columns[annotation.columns.str.startswith("Cytogenetics_")], axis="columns")
assert annotation.shape[0] == merged.shape[0]
annotation = annotation.join(merged)

# expand prior therapy
prior_therapies = set(therapy.strip() for therapies in annotation["Prior_Therapy"].dropna().str.split("|") for therapy in therapies)
for therapy in sorted(prior_therapies):
    print(therapy)
    v = annotation["Prior_Therapy"].str.contains(therapy)
    v[v == False] = np.nan
    v[v == True] = 1
    annotation["prior_therapy_{}".format(therapy)] = v

# Refine Age column
def age_levels(age):
    if np.isnan(age):
        return None
    if age < 10:
        return "0-9"
    if age < 18:
        return "10-17"
    if age < 40:
        return "18-39"
    if age < 65:
        return "40-64"
    if age < 80:
        return "65-79"
    return "80+"
annotation["Age"] = annotation["Age"].apply(age_levels)

annotation.to_csv("config/annotation.txt", sep="\t", index=False)

fusion_whitelist = pd.read_excel(config["annotation"], "fusion_whitelist")
fusion_whitelist.to_csv("config/fusion_whitelist.txt", index=False)

import pandas as pd

COLUMNS = ("fusion junction-reads spanning-fragments splice-type left-gene "
           "left-breakpoint right-gene right-breakpoint junction-read-names "
           "spanning-fragment-names").split()


def summarize_fusions(fusion_paths, samples, output_path):
    results = []
    for sample, fusions in zip(samples, fusion_paths):
        fusions = pd.read_table(fusions, sep="\t", index_col=False)
        fusions.columns = COLUMNS
        fusions.insert(1, "sample", sample)
        results.append(fusions)
    results = pd.concat(results)

    groups = results.groupby(["left-gene", "right-gene"])

    results["sum_junction"] = groups["junction-reads"].transform(sum)
    results["sum_spanning"] = groups["spanning-fragments"].transform(sum)
    results.sort(["sum_junction", "sum_spanning"],
                 ascending=False,
                 inplace=True)
    results.drop(["junction-read-names", "spanning-fragment-names"],
                 inplace=True,
                 axis="columns")

    results.to_csv(output_path, sep="\t", index=False)


summarize_fusions(snakemake.input, snakemake.params.samples,
                  snakemake.output[0])

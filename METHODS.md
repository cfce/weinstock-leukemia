# RNA-Seq Analysis

Analysis of RNA-Seq data was implemented as a Snakemake [1] workflow. The complete workflow documenting all used parameters and tools in a reproducible way is available at https://bitbucket.org/cfce/weinstock-leukemia. Paired end RNA-Seq samples were mapped to the human genome (hg19) with STAR 2.4.2a [2]. Transcript expressions were estimated with Cufflinks 2.2.1 without transcript assembly [3]. Gene expressions were calculated as sums of transcript FPKM values. Genes with transcripts smaller than 300bp were ignored. In order to compare the obtained FPKM values with external datasets, namely RNA-Seq RPKMs from the TCGA AML cohort, RMA-normalized log2 scaled microarray intensities from CCLE cohort and RNA-Seq RPKMs from **TODO fill in study name** all expression values were log2 transformed.
The unnormalized gene expressions of all samples follow similar bimodal distributions. A quantile normalization (Limma 3.26.1 [4]) was performed to adjust for library depth and technology specific differences (Figure **TODO insert F1 and F3 from report**).

Supplementary Figure **TODO insert F4 from report** shows that the primary sources of variation after normalization are the different batches. For the histology specific analysis, we therefore removed batch effects within each tumor type using the ComBat approach from SVA 3.18.0 [5] without specifying additional covariates. Supplementary Figure **TODO insert F5 from report** shows that batch effect removal was successful.

Hierarchical clustering with ward-linkage and euclidean distance over the 750 genes with highest variance-to-mean ratio was performed separately for each histology (with external datasets) and over all histologies (only internal datasets).
Fusion genes were called with STAR-Fusion 0.4.0 [6]. Evidence for fusions was measured by summing spanning fragments (read pairs that align on both sides of a breakpoint) and junction reads (reads that align over the breakpoint). Fusions with an evidence of at least 10 over all samples were retained, and further filtered manually to conservatively remove putative homology induced artifacts.

# References
1. Köster, J., & Rahmann, S. (2012). Snakemake — a scalable bioinformatics workflow engine. Bioinformatics, 28(19), 2520–2522.
2. Dobin, A., Davis, C. A., Schlesinger, F., Drenkow, J., Zaleski, C., Jha, S., … Gingeras, T. R. (2013). STAR: ultrafast universal RNA-seq aligner. Bioinformatics (Oxford, England), 29(1), 15–21. http://doi.org/10.1093/bioinformatics/bts635
3. Trapnell, C., Williams, B. A., Pertea, G., Mortazavi, A., Kwan, G., van Baren, M. J., … Pachter, L. (2010). Transcript assembly and quantification by RNA-Seq reveals unannotated transcripts and isoform switching during cell differentiation. Nature Biotechnology, 28(5), 511–5. http://doi.org/10.1038/nbt.1621
4. Ritchie, M. E., Phipson, B., Wu, D., Hu, Y., Law, C. W., Shi, W., & Smyth, G. K. (2015). limma powers differential expression analyses for RNA-sequencing and microarray studies. Nucleic Acids Research, 43(7), e47. http://doi.org/10.1093/nar/gkv007
5. Leek, J. T., & Storey, J. D. (2007). Capturing heterogeneity in gene expression studies by surrogate variable analysis. PLoS Genetics, 3(9), 1724–35. http://doi.org/10.1371/journal.pgen.0030161
6. https://star-fusion.github.io

GCT and sample annotation files were downloaded from http://www.broadinstitute.org/ccle/data.
Afterwards, relevant samples were extracted with the python script CCLE-extract-expressions.py.

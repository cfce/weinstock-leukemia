import pandas as pd
d = pd.read_table("CCLE_Expression_Entrez_2012-09-29.gct", skiprows=2)
d = d[d.columns[1:]]
d = d.dropna()
d.columns = ["gene"] + list(d.columns[1:])
d.to_csv("CCLE_Expression_Entrez_2012-09-29.txt", sep="\t", index=False)

import glob
import pandas as pd


exprs = pd.concat([pd.read_table(f, index_col=0, squeeze=False) for f in glob.glob("/cfce1/projects/Weinstock_Leukemia/RNA-Seq/REF_data/BA-REF_data/SRR*/Summary/Expression_gene.txt")], axis=1)

exprs.columns = [s.split("_")[0] for s in exprs.columns]

exprs.to_csv("gene_expressions.txt", sep="\t")

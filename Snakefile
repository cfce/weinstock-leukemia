import csv
import re
import pandas as pd
from pandas.util.testing import assert_frame_equal
from snakemake.utils import report
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider


configfile: "config/config.yaml"


HTTP = HTTPRemoteProvider()

try:
    annotation = pd.read_table("config/annotation.txt", index_col=0)
except OSError:
    print("Annotation file config/annotation.txt does not exist. Run scripts/update_annotation.py first.", file=sys.stderr)
    exit(1)

types = list(annotation.loc[annotation["Include_in_Analysis_and_Figures"] == 1, "group"].dropna().unique())


def is_paired(sample):
    f = config["samples"][sample]
    return isinstance(f, list)


def get_bam(wildcards):
    f = config["samples"][wildcards.sample]
    if isinstance(f, str) and f.endswith(".bam"):
        return f
    return "mapped/{}/Aligned.sortedByCoord.out.bam".format(wildcards.sample)


rule all:
    input:
        "results/report.html",
        "results/{}.rpkm.xlsx".format(config["name"]),


rule star:
    input:
        fastq=lambda wildcards: config["samples"][wildcards.sample]
    output:
        "mapped/{sample}/Aligned.sortedByCoord.out.bam",
        "mapped/{sample}/Chimeric.out.junction",
        "mapped/{sample}/Chimeric.out.sam"
    params:
        prefix="mapped/{sample}/"
    log:
        "logs/star/{sample}.log"
    threads: 24
    shell:
        "STAR --genomeDir {config[star-index]} --readFilesIn {input} "
        "--readFilesCommand zcat --runThreadN {threads} "
        "--outFileNamePrefix {params.prefix} "
        "--outSAMstrandField intronMotif "
        "--outFilterIntronMotifs RemoveNoncanonicalUnannotated "
        "--outReadsUnmapped None --chimSegmentMin 12 "
        "--chimJunctionOverhangMin 12 "
        "--alignSJDBoverhangMin 10 "
        "--alignMatesGapMax 200000 "
        "--alignIntronMax 200000 "
        "--outSAMtype BAM SortedByCoordinate &> {log}"


rule star_fusion:
    input:
        junc="mapped/{sample}/Chimeric.out.junction",
        sam="mapped/{sample}/Chimeric.out.sam",
        gtf=config["transcripts"]["annotation"],
    output:
        "fusion_genes/{sample}/{sample}.fusion_candidates.final"
    params:
        prefix=os.path.abspath("fusion_genes/{sample}/{sample}"),
    log:
        "logs/star-fusion/{sample}.log"
    run:
        gtf = os.path.abspath(input.gtf)
        junc = os.path.abspath(input.junc)
        sam = os.path.abspath(input.sam)
        shell("STAR-Fusion --out_prefix {params.prefix} --ref_GTF {gtf} "
              "--chimeric_junction {junc} "
              "--chimeric_out_sam {sam} &> {log}")


rule extract_breakpoints:
    input:
        "fusion_genes/{sample}/{sample}.fusion_candidates.final"
    output:
        "fusion_genes/{sample}/{sample}.breakpoints.txt"
    run:
        fusions = pd.read_table(input[0], )
        lbp = fusions["LeftBreakpoint"].str.split(":", expand=True)
        rbp = fusions["RightBreakpoint"].str.split(":", expand=True)
        breakpoints = pd.concat([lbp.loc[:, [0, 1]], rbp.loc[:, [0, 1]]], axis=1)
        breakpoints["tissue"] = config["tissue"]
        breakpoints.to_csv(output[0], sep="\t", header=False, index=False)


rule oncofuse:
    input:
        "fusion_genes/{sample}/{sample}.breakpoints.txt"
    output:
        "fusion_genes/{sample}/{sample}.oncofuse.txt"
    log:
        "logs/oncofuse/{sample}.log"
    threads: 2
    shell:
        "java -jar ../../oncofuse/oncofuse-1.0.9b2/Oncofuse.jar -a {config[assembly]} "
        "-p {threads} {input} coord - {output} &> {log}"


rule summarize_fusions:
    input:
        expand("fusion_genes/{sample}/{sample}.fusion_candidates.final", sample=filter(is_paired,config["samples"])),
    output:
        "results/fusion_genes.txt"
    params:
        samples=list(filter(is_paired,config["samples"]))
    script:
        "scripts/summarize-fusions.py"


rule plot_fusions:
    input:
        fusions="results/fusion_genes.txt",
        annotation="config/annotation.txt",
        whitelist="config/fusion_whitelist.txt"
    output:
        pdf="results/{covariate}.fusion_genes.heatmap.pdf",
        txt="results/{covariate}.fusion_genes.filtered.txt"
    script:
        "scripts/plot-fusions.py"


rule cufflinks_assembly:
    input:
        config["transcripts"]["annotation"],
        get_bam
    output:
        "transcripts/{sample}/transcripts.gtf",
        "transcripts/{sample}/isoforms.fpkm_tracking",
        "transcripts/{sample}/genes.fpkm_tracking"
    log:
        "logs/cufflinks/assembly/{sample}.log"
    params:
        dir="transcripts/{sample}"
    threads: 8
    shell:
        "cufflinks -p {threads} -v --output-dir {params.dir} --GTF-guide {input} 2> {log}"


rule cuffmerge_config:
    input:
        expand("transcripts/{sample}/transcripts.gtf", sample=config["samples"])
    output:
        "merged_transcripts/config.txt"
    shell:
        r"echo {input} | tr ' ' '\n' > {output}"


rule cuffmerge:
    input:
        config["transcripts"]["annotation"],
        "merged_transcripts/config.txt"
    output:
        "merged_transcripts/merged.gtf"
    log:
        "logs/cuffmerge.log"
    params:
        dir="merged_transcripts"
    threads: 24
    shell:
        "cuffmerge -p {threads} --output-dir {params.dir} --ref-gtf {input} 2> {log}"


rule cufflinks_quantify:
    input:
        "merged_transcripts/merged.gtf" if config["transcripts"]["assembly"] else config["transcripts"]["annotation"],
        get_bam
    output:
        "expressions/{sample}/isoforms.fpkm_tracking"
    log:
        "logs/cufflinks/quantify/{sample}.log"
    params:
        dir="expressions/{sample}"
    shell:
        "cufflinks -v --output-dir {params.dir} --GTF {input} 2> {log}"


rule calc_gene_expressions:
    input:
        expand("expressions/{sample}/isoforms.fpkm_tracking", sample=config["samples"])
    output:
        "expressions/all.fpkm.txt"
    params:
        names=config["samples"]
    script:
        "scripts/calc-gene-expressions.py"


rule merge_gene_expressions:
    input:
        expr="expressions/all.fpkm.txt",
        rpkm=config["gene-expressions"]["external-data"]["rpkm"].values(),
        rma=config["gene-expressions"]["external-data"]["rma"].values(),
        annotation="config/annotation.txt"
    output:
        txt="expressions/all+external.log2.txt",
        overlap="results/merged_expressions.gene_overlap.pdf",
        sample_sources="expressions/all+external.sample_sources.txt"
    params:
        labels=(
            [config["name"]] +
            list(config["gene-expressions"]["external-data"]["rpkm"]) +
            list(config["gene-expressions"]["external-data"]["rma"])
        )
    script:
        "scripts/merge-gene-expressions.py"


rule plot_merged_expression_dist:
    input:
        annotation="config/annotation.txt",
        expr="expressions/all+external.log2.txt"
    output:
        "results/merged_expressions.dist.pdf"
    script:
        "scripts/plot-expression-dist.py"


rule normalize:
    input:
        "expressions/all+external.log2.txt"
    output:
        "normalized_expressions/all+external.log2.txt"
    script:
        "scripts/normalization.R"


rule rpkm_matrix:
    input:
        expr="normalized_expressions/all+external.log2.txt",
        annotation="config/annotation.txt"
    output:
        "results/{cohort}.rpkm.txt"
    script:
        "scripts/rpkm_matrix.py"


rule plot_normalized_expressions_dist:
    input:
        annotation="config/annotation.txt",
        expr="normalized_expressions/all+external.log2.txt"
    output:
        "results/normalized_expressions.dist.pdf"
    script:
        "scripts/plot-expression-dist.py"


rule extract_covariate:
    input:
        annotation="config/annotation.txt",
        exprs="normalized_expressions/all+external.log2.txt"
    output:
        "normalized_expressions/{covariate,(?!all+external).+}.log2.txt"
    run:
        import pandas as pd
        exprs = pd.read_table(input.exprs, index_col=0)
        ann = pd.read_table(input.annotation)
        samples = ann.loc[ann["group"] == wildcards.covariate, "sample"].dropna()
        exprs = exprs.loc[:, samples].dropna(axis=1)
        exprs.to_csv(output[0], sep="\t")


rule batch_effect_removal:
    input:
        annotation="config/annotation.txt",
        expr="normalized_expressions/{covariate}.log2.txt"
    output:
        txt="nobatch_expressions/{covariate}.log2.txt",
        pdf="results/{covariate}.combat_qc.pdf"
    params:
        batch_column="cohort"
    script:
        "scripts/combat.R"


def get_annotation_columns(wildcards):
    columns = list(config["clustering"]["annotation"])
    if wildcards.covariate in config["clustering"]:
        columns += config["clustering"][wildcards.covariate]["annotation"]
    return columns


rule clustering:
    input:
        annotation="config/annotation.txt",
        expr="nobatch_expressions/{covariate}.log2.txt",
    output:
        plot="results/{covariate}.clustering.pdf",
        sample_order="results/{covariate}.clustering.sample_order.txt",
        gene_order="results/{covariate}.clustering.gene_order.txt"
    params:
        label=config["clustering"]["label"],
        genes=[],
        samples=[],
        annotation_columns=get_annotation_columns
    script:
        "scripts/hierarchical-clustering.R"


rule oncoprint:
    input:
        annotation="config/annotation.txt"
    output:
        plot="results/{covariate}.oncoprint.pdf"
    params:
        label=config["clustering"]["label"],
        samples=lambda wildcards: list(annotation.loc[(annotation["group"] == wildcards.covariate) & (annotation["Include_in_Analysis_and_Figures"] == 1) & (annotation["cohort"] == config["name"])].index)
    script:
        "scripts/oncoprint.R"

rule clustering_internal:
    input:
        annotation="config/annotation.txt",
        expr="normalized_expressions/all+external.log2.txt",
    output:
        plot="results/all.clustering.pdf",
        sample_order="results/all.clustering.sample_order.txt",
        gene_order="results/all.clustering.gene_order.txt"
    params:
        label=config["clustering"]["label"],
        genes=[],
        samples=list(map(str, annotation[annotation["cohort"] == config["name"]].index)),
        annotation_columns=config["clustering"]["all"]["annotation"],
    script:
        "scripts/hierarchical-clustering.R"


rule pca:
    input:
        annotation="config/annotation.txt",
        expr="{stage}_expressions/{covariate}.log2.txt"
    output:
        "results/{covariate}.pca.{stage}.pdf"
    params:
        annotation_column="cohort"
    script:
        "scripts/pca.py"


rule show_genes:
    input:
        annotation="config/annotation.txt",
        expr="nobatch_expressions/{covariate}.log2.txt"
    output:
        plot="results/{covariate}.interesting-genes.pdf",
        sample_order="results/{covariate}.interesting-genes.sample_order.txt",
        gene_order="results/{covariate}.interesting-genes.gene_order.txt"
    params:
        annotation_columns=config["clustering"]["annotation"],
        label=config["clustering"]["label"],
        genes=config["gene-expressions"]["interesting-genes"],
        samples=[],
    script:
        "scripts/hierarchical-clustering.R"


rule txt_to_xlsx:
    input:
        "{prefix}.txt"
    output:
        "{prefix}.xlsx"
    run:
        pd.read_table(input[0]).to_excel(output[0])


rule report:
    input:
        F1="results/merged_expressions.dist.pdf",
        F2="results/merged_expressions.gene_overlap.pdf",
        F3="results/normalized_expressions.dist.pdf",
        F4="results/all+external.pca.normalized.pdf",
        T1="results/fusion_genes.xlsx",
        F5=expand("results/{covariate}.pca.nobatch.pdf", covariate=types),
        F6=expand("results/{covariate}.clustering.pdf", covariate=["all"] + types),
        F7=expand("results/{covariate}.fusion_genes.heatmap.pdf", covariate=types),
        T2=expand("results/{covariate}.fusion_genes.filtered.xlsx", covariate=types),
        #F8=expand("results/{covariate}.interesting-genes.pdf", covariate=types),
        T4=os.path.splitext("config/annotation.txt")[0] + ".xlsx",
        T5=expand("results/{covariate}.clustering.sample_order.xlsx", covariate=["all"] + types),
        T6=expand("results/{covariate}.clustering.gene_order.xlsx", covariate=["all"] + types),
        F8=expand("results/{covariate}.oncoprint.pdf", covariate=types)
    output:
        "results/report.html"
    run:
        report("""
        =======================================================
        RNA-seq analysis of leukemia patient derived xenografts
        =======================================================

        The goal of the project is to reveal similarities or differences
        between a leukemia xenograft cohort, TCGA primary tumors, and
        related cell-lines.
        Table T4_ provides a list of samples with the corresponding annotation.

        Step 1: mapping and gene expression estimation
        ----------------------------------------------
        RNA-Seq reads of the xenograft samples were mapped with STAR,
        and transcripts were assembled with Cufflinks. Assembled transcripts
        were merged and unified with Currmerge. Then, transcript abundances
        were estimated
        with Cufflinks as FPKM values. Gene expressions were calculated as
        sum of transcript expressions (see A1_). Transcripts smaller than 300bp
        were ignored as they cannot be reliably measured with RNA-Seq (see L1_).
        This includes miRNA and snoRNA.
        We chose to use FPKM values over read count based approaches as they are
        independent of gene length.
        This is important here, since expressions shall be combined with microarray
        intensities.

        Step 2: combination with external data
        --------------------------------------
        Next, obtained FPKM values were merged with external data from TCGA
        (RNA-Seq RPKMs) and CCLE (RMA-normalized log2-scaled microarray intensities).
        FPKM/RPKM values were log2-transformed. Figure F1_ shows the resulting
        distributions. Only genes that have been measured in all three cohorts were
        considered in the following (Figure F2_ shows common and exclusive genes).

        Step 3: normalization
        ---------------------
        Since the distributions are of similar shape, gene expressions were normalized
        between samples with quantile normalization. As expected, the resulting
        distributions are equal (Figure F3_).

        Step 4: batch effect removal
        ----------------------------
        Since expressions come from three different cohorts, batch effects can be
        expected. This can be seen in a PCA (expressions scaled and centered), where
        samples are clearly separated by cohort (Figure F4_).
        We use the ComBat empirical bayes approach in
        parametric mode to remove batch effects.
        Since Leukemia types are confounded with the batches (e.g. TCGA data consists
        only of AML samples), we remove the batch effects within each tumor type (Figures F5_).

        Step 5: clustering
        ------------------
        Next, hierarchical clustering with ward-linkage and euclidean distance
        over the 500 genes with highest variance-to-mean ratio was performed for each tumor type and over all types (Figure F6_).
        Table T5_ and T6_ give the corresponding sample and gene names of rows and columns of the clustering heatmaps.
        Separate oncoprints for all types can be found in Figure F8_.

        Step 6: fusion genes
        --------------------
        From the available paired-end samples, fusion genes were called with STAR-Fusion (Table T1_) with default parameters.
        The resulting table is sorted by evidence, listing the fusion candidates with highest support at the top.
        Figure F7_ visualizes the highest evidence fusion genes.
        Table T2_ shows the same fusions in Excel format.

        .. _A1: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3334321
        .. _L1: http://seqanswers.com/forums/archive/index.php/t-17404.html
        """, output[0], metadata="Johannes Köster (koester@jimmy.harvard.edu)", **input)

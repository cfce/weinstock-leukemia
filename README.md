[![Snakemake](https://img.shields.io/badge/snakemake-≥3.5.2-brightgreen.svg?style=flat-square)](http://snakemake.bitbucket.org) [![bioconda-badge](https://img.shields.io/badge/install%20with-bioconda-brightgreen.svg?style=flat-square)](http://bioconda.github.io)

# RNA-Seq analysis of leukemia patient derived xenografts

This Snakemake workflow implements the RNA-Seq analysis of 107 leukemia patient derived xenografts (PDX) as presented in

> Townsend, Murakami et al. "The Public Repository of Xenografts (ProXe) enables discovery and randomized phase II-like trials in mice" (in preparation)

The workflow is able to fully automatically reproduce the RNA-Seq results presented in the article.

The goal of the analysis was to characterize the patient derived xenografts (PDX) in terms of gene expression and fusion genes. Subtypes were be compared against external patient samples and cell lines. Here, batch effects had to be taken into account. Further, different types of data (RNA-seq and microarray) had to be integrated.
The workflow consits of the following major steps:

* Download the FASTQ files from SRA.
* Perform the mapping.
* Call fusion genes.
* Quantify gene expressions.
* Normalize gene expressions.
* Remove batch effects.
* Perform unsupervised clustering analyses.

The implementation (including all parameters and settings) can be inspected [online](https://bitbucket.org/cfce/weinstock-leukemia/src).

## Usage

The workflow is designed for Linux systems.
**The following instructions will work with the final version of the workflow once the manuscript is published and the RNA-Seq data is available on SRA.**

### Step 1: Install Miniconda3

The workflow uses various Python and R packages and command line tools. All needed software is available via [Bioconda](https://bioconda.github.io). To use Bioconda, please install Miniconda3:

```
#!bash

wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

Answer ``yes`` to this question:

```
Do you wish the installer to prepend the Miniconda3 install location to PATH ...? [yes|no]
```

### Step 2: Download and extract the workflow

You can check out the workflow from this repository using [Git](https://git-scm.com) or download it directly:

```
#!bash

curl https://bitbucket.org/cfce/weinstock-leukemia/get/master.tar.gz | tar xvz
cd cfce-weinstock-leukemia*
```


### Step 3: Install software from Bioconda

All required software can be installed with

```
conda install --channel bioconda --file requirements.txt
```

### Step 4: Run the workflow

A dry-run of the workflow can be performed with

```
#!bash

snakemake --dryrun
```

The workflow can be executed with

```
#!bash

snakemake --cores N
```

where N is the number of processor cores to use. Be sure to perform this on a powerful machine and expect it to take a lot of time. Alternatively you can use a cluster system. For details see http://snakemake.bitbucket.org.